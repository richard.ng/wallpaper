import { Image, Website, WebsiteIndex } from './interface';
import Scraper from './scraper';
import Downloader from './downloader';
import Setter from './setter';

let websites: WebsiteIndex = {
  'reddit': {
    url: 'https://www.reddit.com/r/wallpapers/hot',
    rawRegex: 'preview.*crop',
    cleanRegex: 'redd\\.it\\/(.*)\\?',
    slug: 'https://i.redd.it/'
  },
  'unsplash': {
    url: 'https://unsplash.com/wallpapers',
    rawRegex: 'images.unsplash.com/photo',
    cleanRegex: '(photo.*)\\?',
    slug: 'https://images.unsplash.com/'
  },
}

const dir = `${process.cwd()}/temp/`

async function scrape() {
  let downloader = new Downloader(dir)

  let scrapers = {
    'reddit': new Scraper(websites.reddit),
    'unsplash': new Scraper(websites.unsplash)
  }

  function scraperMethods(func: string, context: {[index: string]: Scraper}) {
    return context[func]
  }

  let downloadList: Image[] = []
  for (let scraper in scrapers) {
    let rawData = await scraperMethods(scraper, scrapers).getRawImageUrls()
    let cleanData = await scraperMethods(scraper, scrapers).cleanImageUrls(<string[]>rawData)
    downloadList = downloadList.concat(cleanData)
  }

  for (let each of downloadList) {
    await downloader.download(each)
  }

  return new Promise((resolve) => {
    console.log('done scraping')
    resolve('success')
  })
}

async function set() {
  console.log('setting')
  let setter = new Setter(dir)
  await setter.getList()
  setter.set()
}

async function main() {
  function timer(sec: number) {
    setInterval(async () => {
      await scrape()
    }, sec * 1000)
  }
  await scrape()
  await set()
  timer(43200) // 43200 sec = 12 hr
}

main()
