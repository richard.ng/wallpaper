import fs from 'fs';
import * as proc from 'child_process';

class Setter {
  dir: string
  files: string[] = []
  final: string[] = []

  constructor(dir: string) {
    this.dir = dir
  }

  sleep(sec: number) {
    return new Promise(resolve => setTimeout(resolve, sec*1000));
  }

  randomize() {
    let workinglist = this.files
    let randomizedlist = []
    for (let i = 0; i < this.files.length; i++) {
      let chosen = workinglist[Math.floor(Math.random() * workinglist.length)]
      randomizedlist.push(chosen)
      workinglist = workinglist.filter(item=>item!==chosen)
    }
    return randomizedlist
  }

  multiply(number: number) {
    for (let i = 0; i < number; i++) {
      this.randomize().forEach(item=>this.final.push(item))
    }
  }

  async getList() {
    this.files = await fs.readdirSync(this.dir)
    this.randomize()
    this.multiply(Math.floor(50/this.files.length))
  }

  async set() {
    for (let i = 0; i < this.final.length; i++) {
      let date = new Date()
      let file=`${process.cwd()}/temp/${this.final[i]}`
      console.log(`${date} - ${this.final[i]}`)
      proc.exec(`/usr/bin/gsettings set org.gnome.desktop.background picture-uri ${file}`)
      await this.sleep(1800) // 1800 sec = 30 min
    }
  }
}

export default Setter;
