import { Image } from './interface';
import axios from 'axios';
import fs from 'fs';

class Downloader {
  dir: string

  constructor(dir: string) {
    this.dir = dir
    if (fs.existsSync(this.dir)) {
      fs.rmSync(this.dir, {recursive: true})
    }
    fs.mkdirSync(this.dir)
  }

  async download(image: Image) {
    let filename = `${this.dir}/${image.name}`
    let imageData = await axios.get(image.url, {responseType: 'stream'})
    imageData.data.pipe(fs.createWriteStream(filename))
  }
}

export default Downloader;
