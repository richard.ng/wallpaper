import axios from 'axios';
import cheerio from 'cheerio';
import { Image, Website } from './interface';

class Scraper {
  axiosInstance = axios.create()
  rawList: string[] = []
  cleanList: Image[] = []
  website: Website

  constructor(website: Website) {
    this.website = {
      url: website.url,
      rawRegex: website.rawRegex,
      cleanRegex: website.cleanRegex,
      slug: website.slug
    }
  }

  async getRawImageUrls() {
    let html = await this.axiosInstance.get(this.website.url)
    let $ = cheerio.load(html.data)
    $('img').each((i,e) => {
      if (this.rawList.length > 10) { return }
      let url = $(e).attr('src')!.match(this.website.rawRegex)
      if (url !== null) {
        this.rawList.push(<string>url['input'])
      }
    })

    return this.rawList
  }

  cleanImageUrls(rawList: string[]) {
    for (let i = 0; i < rawList.length; i++) {
      let match = rawList[i].match(this.website.cleanRegex)![1]

      let image = {
        url: this.website.slug + match,
        name: match
      }
      this.cleanList.push(image)
    }
    return this.cleanList
  }
}
export default Scraper;
