export interface Image {
  url: string,
  name: string
}

export interface Website {
  url: string,
  rawRegex: string,
  cleanRegex: string
  slug: string
}

export interface WebsiteIndex {
  [index: string]: Website
}
